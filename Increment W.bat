@echo off
setlocal enableDelayedExpansion
set "baseName=SB_Vote_Tally W V1."
set "n=0"
for /f "delims=" %%F in (
  '2^>nul dir /b "%baseName%*.zip"^|findstr /xri /c:"%baseName%[0-9]*.zip"'
) do (
  set "name=%%F"
  set "name=!name:*%baseName%=!"
  if !name! gtr !n! set "n=!name!"
)
set m=%n%
set /a n+=1
copy "Vote Tally.zip" "%baseName%%n%.zip"
del "%baseName%%m%"